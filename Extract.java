import java.util.Scanner;
import java.io.BufferedReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.FileWriter;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Extract {
	public static void main(String args[]) {
		FileInputStream fis = null;
		
		BufferedReader reader = null;
		
		Scanner console = new Scanner(System.in);
		BufferedWriter outFile = null;
		BufferedWriter outDataset2 = null;
		BufferedWriter outDataset1 = null;
	    

		
		
		
		
		
		Matcher matcher, matcherGroup;
		
		try {
			System.out.println("Which dataset do you want to extract?");
			String i;
			File folder = new File(System.getProperty("user.dir"));
		    if (folder.isDirectory()) {
		      File[] listOfFiles = folder.listFiles();
		      for (File file : listOfFiles) {
		          if (file.getName().contains("_stage3_allGPM.xml"))
		    	  System.out.println(file.getName().replaceFirst("_stage3_allGPM.xml", ""));
		      }
		    }
			
			
			String tempData=console.nextLine().toString()+"_";
			
			fis = new FileInputStream(tempData+"stage3_allGPM.xml");
			
			reader = new BufferedReader(new InputStreamReader(fis));
			
			outFile = new BufferedWriter(new FileWriter(tempData+"file-Name.txt"));
			
			outDataset1 = new BufferedWriter(new FileWriter(tempData+"attributes1.txt"));
			outDataset2 = new BufferedWriter(new FileWriter(tempData+"attributes2.txt"));
			
			String line = reader.readLine();
			String att1="";
			String att2="";
			
			String dataset= "";
			String group = "";
			
			
			Pattern patternFile = Pattern
					.compile("<File name=\"(.*?)\">");
			Pattern patternDataset = Pattern
					.compile("<Dataset name=\"(.*?)\">");
			Pattern patternSlash = Pattern.compile("(.*?)/");
			
			Pattern patternCapital = Pattern.compile("^([A-Z]*)");
			Pattern patternCapital2 = Pattern.compile("^([A-Z][A-Z]*)");
			Pattern patternSplit = Pattern.compile("([A-Z]?[a-z|0-9]*)");
			while (line != null) {
				
				
				matcher = patternFile.matcher(line);

				if (matcher.find()) {
					
					
					outFile.write(matcher.group(1));
					
					line = reader.readLine();
					while (!line.contains("</File>")) {
						matcher = patternDataset.matcher(line);
						if (matcher.find()) {
							dataset=matcher.group(1);
							matcher = patternSlash.matcher(dataset);
							if (matcher.find())
							{
								group= matcher.group(1);
								dataset=matcher.replaceFirst("");
								
								matcherGroup = patternCapital2.matcher(group);
								while (matcherGroup.find())
								{
									
									if (!att1.contains(matcherGroup.group(1)))
									{
										att2=att2+matcherGroup.group(1)+" ";
									}
									att1=att1+matcherGroup.group(1)+" ";
								}
								
								matcherGroup = patternSplit.matcher(group);
								while (matcherGroup.find())
								{
									
									if (!att1.contains(matcherGroup.group(1)))
									{
										att2=att2+matcherGroup.group(1)+" ";
									}
									att1=att1+matcherGroup.group(1)+" ";
								}
			
							}
							
							matcher = patternCapital.matcher(dataset);
							if (matcher.find())
							{
								
								if (!att1.contains(matcher.group(1)))
								{
									att2=att2+matcher.group(1)+" ";
								}
								att1=att1+matcher.group(1)+" ";
								dataset=matcher.replaceAll("");
							}
							matcher = patternSplit.matcher(dataset);
							while (matcher.find())
							{
								
								if (!att1.contains(matcher.group(1)))
								{
									att2=att2+matcher.group(1)+" ";
								}
								att1=att1+matcher.group(1)+" ";
		
							}
							
							
		
						}
						outDataset1.write(att1);
						outDataset2.write(att2);
						line = reader.readLine();
						att1="";
						att2="";
					}
					
					outFile.newLine();
					outDataset1.newLine();
					outDataset2.newLine();
				}

				line = reader.readLine();
			}

			outFile.close();
			outDataset2.close();
			outDataset1.close();
			
		} catch (FileNotFoundException ex) {
			Logger.getLogger(BufferedReader.class.getName()).log(Level.SEVERE,
					null, ex);
		} catch (IOException ex) {
			Logger.getLogger(BufferedReader.class.getName()).log(Level.SEVERE,
					null, ex);
		} finally {
			try {
				reader.close();
			
				fis.close();
			
			} catch (IOException ex) {
				Logger.getLogger(BufferedReader.class.getName()).log(
						Level.SEVERE, null, ex);
			}
		}
	}
}
