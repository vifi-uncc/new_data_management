import java.util.Scanner;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.File;

import org.apache.lucene.index.Term;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.SortedSetDocValues;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.PrefixQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
public class Search {
	 public static void main(String[] args) throws IOException, ParseException {
	        // 0. Specify the analyzer for tokenizing text.
	        //    The same analyzer should be used for indexing and searching
	        StandardAnalyzer analyzer = new StandardAnalyzer();

	        // 1. create the index
	        Directory index_data = new RAMDirectory();
	        IndexWriterConfig config_data = new IndexWriterConfig(analyzer);
	        IndexWriter w_data = new IndexWriter(index_data, config_data);
	        Directory index_file = new RAMDirectory();
	        IndexWriterConfig config_file = new IndexWriterConfig(analyzer);
	        IndexWriter w_file = new IndexWriter(index_file, config_file);
	        Scanner console = new Scanner(System.in);
	        
	        FileInputStream fis_file = null;
	        FileInputStream fis_data = null;
	        BufferedReader reader_file= null;
	        BufferedReader reader_data= null;
	        FileInputStream fis_query = null;
	        BufferedReader reader_query = null;
	        
	        

	        
	        
	    	System.out.println("Which dataset do you want to extract?");
			String ij;
			File folder = new File(System.getProperty("user.dir"));
		    if (folder.isDirectory()) {
		      File[] listOfFiles = folder.listFiles();
		      for (File file : listOfFiles) {
		          if (file.getName().contains("_stage3_allGPM.xml"))
		    	  System.out.println(file.getName().replaceFirst("_stage3_allGPM.xml", ""));
		      }
		    }
			
			
			String tempData=console.nextLine().toString()+"_";
			
	        float merged_score = 0;
	        BufferedWriter out=null;
			out = new BufferedWriter(new FileWriter(tempData+"search-result.json"));
			try {
				fis_file = new FileInputStream(tempData+"file-Name.txt");
				
				reader_file = new BufferedReader(new InputStreamReader(fis_file));
				
				fis_data = new FileInputStream(tempData+"attributes2.txt");
				fis_query = new FileInputStream("Query.txt");
				reader_data = new BufferedReader(new InputStreamReader(fis_data));
				reader_query = new BufferedReader(new InputStreamReader(fis_query));
				
				
				String line_index_data;
				line_index_data = reader_data.readLine();
				
				String line_index_file;
				line_index_file = reader_file.readLine();
				while (line_index_data != null) {
				
					 addDoc(w_data, line_index_data, "");
					line_index_data = reader_data.readLine();
					addDoc(w_file, line_index_file, "");
					line_index_file = reader_file.readLine();
					
				}
				
	        w_data.close();
	        w_file.close();
	        
	        System.out.println("Please Enter your keyword or \"Query\" to use the Query file:");
			ij=console.nextLine().toString();
	        
			
			
			
			if (ij.equals("Query")){
	        String line_query;
	        line_query=reader_query.readLine();
	        while(line_query!=null){
	        // 2. query
	        

	        // the "title" arg specifies the default field to use
	        // when no field is explicitly specified in the query.
	        Query q_query = new QueryParser("title", analyzer).parse(QueryParser.escape(line_query));
	        
	        
	        IndexReader reader_index_data = DirectoryReader.open(index_data);
	        IndexSearcher searcher_data = new IndexSearcher(reader_index_data);
	        TopDocs docs_data = searcher_data.search(q_query, reader_index_data.numDocs());
	        ScoreDoc[] hits_data = docs_data.scoreDocs;
	        IndexReader reader_index_file = DirectoryReader.open(index_file);
	        IndexSearcher searcher_file = new IndexSearcher(reader_index_file);
	       
	        int hitsPerPage = hits_data.length;
	        if (hitsPerPage==0)
	        {
	        	System.out.println("Is "+line_query+" a prefix?y/n");
				ij=console.nextLine().toString();
				if(ij.equals("y"))
				{
					Term term = new Term("title", line_query);
					Query query = new PrefixQuery(term);
					docs_data = searcher_data.search(query, reader_index_data.numDocs());
			        hits_data = docs_data.scoreDocs;
				}
				else
				{
					System.out.println("Do you want to search for prefixes?y/n");
					ij=console.nextLine().toString();
					if(ij.equals("y"))
					{
						
					}
				}
	        }
	        
	        
	        
	        int doc_number[]=new int[hitsPerPage];
	        float doc_score[]=new float[hitsPerPage];
	        
	        for(int i=0;i<hits_data.length;++i) {
	        	merged_score= hits_data[i].score/docs_data.getMaxScore();
	        
	        doc_number[i]=hits_data[i].doc;
	        doc_score[i]=merged_score;
	        }
	        
	        out.write(hitsPerPage+" results for : "+ line_query);
	        //System.out.println(hitsPerPage+" results for : "+ line_query);
            out.newLine();
	        for(int i=0;i<hitsPerPage;++i) {
	            int docId = doc_number[i];
	            //if (searcher_type.doc(docId).get("title").contains("[diap]")||searcher_type.doc(docId).get("title").contains("[topp]")||searcher_type.doc(docId).get("title").contains("[hlca]")||searcher_type.doc(docId).get("title").contains("[lbpr]"))
		        
	            Document file = searcher_data.doc(docId);
	            Document data= searcher_file.doc(docId);
	            //out.write((docId + 1)+":\t" +data.get("title"));
	            System.out.println((docId + 1)+":\t" +data.get("title"));
	            //out.newLine();
	            out.write("{\"rpg"+(docId + 1)+"\":\n[\n{\n\"ip\":\"http://35.167.35.244:8090/nifi\",\n\"name\":\""+data.get("title")+"\",\n\"in_port\":[{\n\"id\":\"ee925818-a935-4eef-b4dc-28f6612e1265\",\n\"comment\":\"JPL Model @ B\",\n\"transmitting\":true\n}]\n}\n]\n}");
	            
	            
	            
	            //out.write((docId + 1)+":\t" +data.get("title")+"\n"+ file.get("title"));
	            out.newLine();
		        
	        }
	        // reader can only be closed when there
	        // is no need to access the documents any more.
	        reader_index_data.close();
	        
	        line_query=reader_query.readLine();
	        
	        }}
			else{
		        String line_query;
		        line_query=ij;
		        
		        Query q_query = new QueryParser("title", analyzer).parse(QueryParser.escape(line_query));
		        
		        
		        IndexReader reader_index_data = DirectoryReader.open(index_data);
		        IndexSearcher searcher_data = new IndexSearcher(reader_index_data);
		        TopDocs docs_data = searcher_data.search(q_query, reader_index_data.numDocs());
		        ScoreDoc[] hits_data = docs_data.scoreDocs;
		        IndexReader reader_index_file = DirectoryReader.open(index_file);
		        IndexSearcher searcher_file = new IndexSearcher(reader_index_file);
		       
		        int hitsPerPage = hits_data.length;
		        if (hitsPerPage==0)
		        {
		        	System.out.println("Is "+line_query+" a prefix?y/n");
					ij=console.nextLine().toString();
					if(ij.equals("y"))
					{
						Term term = new Term("title", line_query);
						Query query = new PrefixQuery(term);
						docs_data = searcher_data.search(query, reader_index_data.numDocs());
				        hits_data = docs_data.scoreDocs;
				        hitsPerPage = hits_data.length;
					}
					else
					{
						System.out.println("Do you want to search for prefixes?y/n");
						ij=console.nextLine().toString();
						if(ij.equals("y"))
						{
							
						}
					}
		        }
		        
		        
		        
		        
		        int doc_number[]=new int[hitsPerPage];
		        float doc_score[]=new float[hitsPerPage];
		        
		        for(int i=0;i<hits_data.length;++i) {
		        	merged_score= hits_data[i].score/docs_data.getMaxScore();
		        
		        doc_number[i]=hits_data[i].doc;
		        doc_score[i]=merged_score;
		        }
		        
		        //out.write(hitsPerPage+" results for : "+ line_query);
		        System.out.println(hitsPerPage+" results for : "+ line_query);
	            //out.newLine();
		        for(int i=0;i<hitsPerPage;++i) {
		            int docId = doc_number[i];
		            //if (searcher_type.doc(docId).get("title").contains("[diap]")||searcher_type.doc(docId).get("title").contains("[topp]")||searcher_type.doc(docId).get("title").contains("[hlca]")||searcher_type.doc(docId).get("title").contains("[lbpr]"))
			        
		            Document file = searcher_data.doc(docId);
		            Document data= searcher_file.doc(docId);
		            //out.write((docId + 1)+":\t" +data.get("title"));
		            System.out.println((docId + 1)+":\t" +data.get("title"));
		            //out.write((docId + 1)+":\t" +data.get("title")+"\n"+ file.get("title"));
		            //out.newLine();
		            out.write("{\"rpg"+(docId + 1)+"\":\n[\n{\n\"ip\":\"http://35.167.35.244:8090/nifi\",\n\"name\":\""+data.get("title")+"\",\n\"in_port\":[{\n\"id\":\"ee925818-a935-4eef-b4dc-28f6612e1265\",\n\"comment\":\"JPL Model @ B\",\n\"transmitting\":true\n}]\n}\n]\n}");
		            
		            
		            out.newLine();
			        
		        }
		        // reader can only be closed when there
		        // is no need to access the documents any more.
		        reader_index_data.close();
		        
		        
			}
			
			
			
			
			out.close();
			ij=console.nextLine().toString();
			}
			catch (FileNotFoundException ex) {
				 Logger.getLogger(BufferedReader.class.getName()).log(Level.SEVERE, null, ex);
				 } catch (IOException ex) {
				 Logger.getLogger(BufferedReader.class.getName()).log(Level.SEVERE, null, ex);
				 } finally {
				 try {
				 reader_data.close();
				 reader_query.close();
				 fis_data.close();
				 fis_query.close();

				 reader_file.close();
				 
				 fis_file.close();
				 
				 } catch (IOException ex) {
				 Logger.getLogger(BufferedReader.class.getName()).log(Level.SEVERE, null, ex);
				 }
				 }
			}

	    private static void addDoc(IndexWriter w, String title, String isbn) throws IOException {
	        Document doc = new Document();
	        doc.add(new TextField("title", title, Field.Store.YES));

	        // use a string field for isbn because we don't want it tokenized
	        doc.add(new StringField("isbn", isbn, Field.Store.YES));
	        w.addDocument(doc);
	    }
}
